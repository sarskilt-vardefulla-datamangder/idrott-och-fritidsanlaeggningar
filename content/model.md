# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en idrotts- och fritidsanläggning och varje kolumn motsvarar en egenskap för den idrotts- och fritidsanläggningen. 25 attribut är definierade, där de första 5 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige. 


</div>

<div class="note" title="2">


<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**id**](#id)|1|heltal|**Obligatoriskt** - Anger en identifierare för idrotts- och fritidsanläggning.|
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som förvaltar idrotts- och fritidsanläggningen.|
|[**name**](#name)|1|text|**Obligatoriskt** - Anger idrotts- och fritidsanläggningens namn.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|[description](#description)|0..1|text|Obligatoriskt - Anger en kortare beskrivning av idrotts- och fritidsanläggningen. |
|[type](#type)|1|[begrepp](#type)|Obligatoriskt - Anger typen av idrotts- och fritidsanläggning, privatägd eller offentligt ägd. |
|[updated](#updated)|0..1|text|Ange när idrotts- och fritidsanläggningen uppdaterades senast.|
|[yearBuilt](#yearbuilt)|0..1|text|Ange byggår.|
|[area](#area)|0..1|text|Ange yta i kvadratmeter.|
|[measurements](#measurements)|0..1|text|Ange mått i meter - Längd, Bredd, Djup, Höjd (LxWxDxH).|
|[divisible](#divisible)|0..1|boolean|Ange om den är anläggningen har delbara ytor eller ej.|
|[accessibility](#accessibility)|0..1|boolean|Ange om den är tillgänglighetsanpassad.|
|[changingrooms_amount](#changingrooms_amount)|0..1|integer|Ange antal omklädningsrum.|
|[changingrooms_capacity](#changingrooms_capacity)|0..1|integer|Ange totalkapacitet i omklädningsrum.|
|[stand](#stand)|0..1|boolean|Ange om den är anläggningen är delbar eller ej.|
|[spectators_amount](#spectators_amount)|0..1|integer|Ange totalkapacitet på läktare.|
|[booking](#booking)|0..1|[URL](#url)|Bokningssida för att boka idrotts- och fritidsanläggningen.|
|[street](#street)|0..1|text|Ange gatuadress.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Ange postnummer.|
|[city](#city)|0..1|text|Ange postort.|
|[phone](#phone)|0..1|text|Ange telefonnumret med inledande landskod, exempelvis +46 |
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[image](#image)|0..*|[URL](#url)|Ange en länk eller flera länkar till bilder på idrotts- och fritidsanläggningen.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om idrotts- och fritidsanläggningen.|




</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, d.v.s. inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där idrotts- och fritidsanläggningen presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

En unik identifierare för den lediga tomten. 

### source

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### name

Namnet på idrotts- och fritidsanläggningen. Exempelvis "Bjärehovhallen".

### description

Beskrivning av idrotts- och fritidsanläggningen.

### type 

Typen av idrotts- och fritidsanläggning kan anges enligt nedan. Utåtgående länkar är till för att underlätta arbetet samt tydliggöra definitionen för varje typ av dessa idrotts- och fritidsanläggningar. Vi utgår i denna standard att alla idrottsanläggningar ägs antingen av kommunal, idell eller privat aktör.

[City](https://schema.org/City)
[NGO](https://schema.org/NGO)
[Corporation](https://schema.org/Corporation)

### latitude

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges. 

### street

Adressen för idrotts- och fritidsanläggningen.

### postalcode

Postnummer till kommunen.

### city

Kommunen där idrotts- och fritidsanläggningen finns.

### phone 

Telefonnummer.

### email

Funktionsadress till organisationen. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: info@organisation.se

### image 

Ange en länk eller flera länkar till bilder på idrotts- och fritidsanläggningen. Anges med inledande schemat **https://** eller **http://** 

### url 

En ingångssida. Anges med inledande schemat **https://** eller **http://** 
